export const SORTING_OPTIONS = {
  CREATED_DESC: 'created_desc',
  CREATED_ASC: 'created_asc',
  DURATION_DESC: 'duration_desc',
  DURATION_ASC: 'duration_asc',
};
export const DEFAULT_SORTING_OPTION = SORTING_OPTIONS.CREATED_DESC;
